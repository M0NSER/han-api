<?php

namespace App\DataFixtures;

use App\Entity\Event;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;

class AppFixtures extends Fixture
{
    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i < 20; $i++) {
            $product = new Event();

            $startDateTimeInt = mt_rand(1262055681, 1262055681);
            $startDateTime = date("Y-m-d H:i:s", $startDateTimeInt);

            $now = new DateTime();

            $product
                ->setTitle("Event Title " . $i)
                ->setDescription("Description: " . $i)
                ->setLatitude((rand(1, 1000) / 10000))
                ->setLongitude((rand(1, 1000) / 10000))
                ->setCity("City " . $i)
                ->setStartDateTime(new DateTime($startDateTime))
                ->setEndDateTime((new DateTime($startDateTime))->modify("+ 1 hour"))
                ->setUser($i)
                ->setCreatedAt($now->modify("- 5 hour"))
                ->setUpdatedAt($now->modify("- 2 hour"))
                ->setDeletedAt(rand(1, 1000) > 900 ? $now : null);
            $manager->persist($product);
        }


        $manager->flush();
    }
}
